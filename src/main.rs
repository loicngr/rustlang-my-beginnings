fn crypter(text: String, n: i32) {
    println!("{} {}", text, n);
    let mut impair: String = String::new();
    let mut pair: String = String::new();
    let mut chaine: String = String::from(text);

    let mut i = 0;
    while i < n {
        let mut tableau: Vec<&str> = chaine.split("").collect();
        tableau.remove(0);
        tableau.remove(tableau.len() - 1);
        println!("{:?}", tableau);
        let mut cpt = 1;
        for lettre in tableau.iter() {
            if cpt % 2 == 0 {
                // pair
                pair.push_str(lettre);
            } else if cpt % 2 != 0 {
                // impair
                impair.push_str(lettre);
            }
            cpt = cpt + 1;
        }
        chaine = &impair + &pair;
        impair = String::new();
        pair = String::new();
        i = i + 1;
    }
}

fn main() {
    let text: String = String::from("This is a test!");
    crypter(text, 1);
}