pub fn main(chaine: String) -> String {
    let mut result: String = String::from("trop court");
    if chaine.len() > 4 {
        let mut start: String = String::with_capacity(chaine.len() - 4);
        for _i in 0..chaine.len() - 4 {
            start = start + "#";
        }
        result = start + &chaine[chaine.len() - 4..chaine.len()];
    }
    result
}