fn string_to_static_str(s: String) -> &'static str {
    Box::leak(s.into_boxed_str())
}

pub fn ft_main(chaine: String) -> &'static str {
    let s: &'static str = string_to_static_str(chaine);

    let tableau: Vec<i32> = s.split_whitespace().map(|x| x.parse().unwrap()).collect();
    let min_value = tableau.iter().min();

    let max_value = tableau.iter().max();
    println!("{:?}", min_value);
    max_value;
}