pub fn ft_main() {
  let mut result: Vec<i32> = Vec::new();
  for i in 0..=1000 {
    let mut n= i;

    loop {
      if n == 8 {
        result.push(i);
        break;
      }
      else if n > 10 {
        let mut tableau: Vec<i32> = Vec::new();
        while n != 0 {
          let reste: i32 = n % 10;
          tableau.push(reste);
          n = (n / 10) as i32;
        }
        n = 0;
        for c in tableau.iter().rev() {
          let &chiffre: &i32 = c;
          n += chiffre;
        }
      } else {
        break;
      }
    }
  }
  println!("Resultat : {:?}", result);
}